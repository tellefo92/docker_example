FROM gcc:11

WORKDIR /usr/games/

RUN apt update --yes
RUN apt install cowsay --yes

CMD "./cowsay" "Hello" "from" "the" "docker" "image"
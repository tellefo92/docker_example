# Docker example

## Prerequisites
Uses `docker` <br/>

## Usage
To build the image:
```sh
$ docker build . -t cowsay
```
To run:
```sh
$ docker run -it --rm cowsay
```

## Unique use of docker
"By default, a container has no resource constraints and can use as much of a given resource as the host’s kernel scheduler allows." [source](https://docs.docker.com/config/containers/resource_constraints/#:~:text=By%20default%2C%20a%20container%20has%20no%20resource%20constraints%20and%20can%20use%20as%20much%20of%20a%20given%20resource%20as%20the%20host%E2%80%99s%20kernel%20scheduler%20allows.) <br/>
When developing software that is going to run on an embedded device that has constraints on memory and cpu, Docker provides ways to control how much memory or cpu a container can use. That way we set up a testing environment on the development device to make sure that the software can run on the embedded device without throwing memory/cpu exceptions or crashing.

## Maintainers
Tellef Østereng | [@tellefo92](https://gitlab.com/tellefo92)
